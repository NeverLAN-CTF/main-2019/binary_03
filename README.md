# Binary 3

#### Description
Another day, another disgruntled engineer. It seems that the login is working fine, but some portions of the application are broken. Do you think you could fix the the code and retrieve the flag?

#### Flag
flag{AP1S_SH0ULD_4LWAYS_B3_PR0T3CTED}

#### Hints
Where do all the bytes lead?

#### Solution
* debug the application with gdb
* place a breakpoint in main => b main
* execute the program
* at the breakpoint call d() => call d()
* flag returned

